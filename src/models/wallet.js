import { Wallet, provider } from 'bytom'

const blockCenterProvider = new provider.BlockCenterProvider({
  beforeRequest (config) {
    config.headers = Object.assign(config.header || {}, {
      from: 'byone',
      version: window.__bytom && window.__bytom.meta.version,
      device_id: window.__bytom && window.__bytom.settings.clientId
    })
    return config
  }
})
export const wallet = new Wallet({
  blockCenterProvider,
  net: 'mainnet',
  chain: 'bytom'
})

export function setNet(net) {
  wallet.setNet(net)
}

export function setChain(chain) {
  wallet.setChain(chain)
}

window.wallet = wallet
