import bytom from "./bytom";
import { wallet } from './wallet'

let query = {};

query.chainStatus = function() {
  return wallet.getVoteStatus()
  // return bytom.query.getVoteStatus();
};

query.blockStatus = function() {
  // return bytom.query.getblockcount();
  return wallet.getChainStatus()
};

export default query;
